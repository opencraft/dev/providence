import {Single} from "../Single";
import {ctxRender, getSingle} from "../../testHelpers";
import {defaultContextValues} from "../../context";
import {createStore} from "redux-dynamic-modules";
import {act} from "@testing-library/react";

describe("Watches a Single", () => {
  it("Registers a listener for reactivity", async () => {
    const context = defaultContextValues()
    const store = createStore({})
    const controller = getSingle<{ id: number, text: string }>('test.single', {
      endpoint: '#',
      x: {id: 1, text: 'Beep'}
    }, {context, store}).controller
    const result = ctxRender(<Single controller={controller}>{() => <div>{controller.x!.text}</div>}</Single>, {
      context,
      store
    })
    expect(result.getByText('Beep')).toBeTruthy()
    await act(() => controller.updateX({text: 'Boop'}))
    expect(result.getByText('Boop')).toBeTruthy()
  })
})
